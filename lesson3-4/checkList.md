# План 3-4 уроків.

1. інструменти  
1.1. редактор  
1.2. инспектор  
1.3. ruler  
1.4. color picker
1.5. git (перенесено на наступний урок)

2. тег div (division) (перенесено на наступний урок)
3. атрибут style 

4. способи розміщення стилів  
4.1. як атрибут тегу  
4.2. в тегі `<style>`

//  МИ ПОЧАЛИ ТУТ
  
4.3. в файлі css 

5. css  
5.1. селектори  
5.2. наслідування  
5.3. псевдоклас hover 
5.4. border
5.5. margin

//  МИ ЗАКІНЧИЛИ ТУТ