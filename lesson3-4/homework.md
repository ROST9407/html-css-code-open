домашне завдання.
1. Fun news  
зайти на сайт з новинами і використовуючи інспектор (Ctrl+Shift+I) та "Edit as HTML" внести зміни у текст відображений на сайті.  
Скріншот скинути в Телеграм.  
(\*) змінити background-color  
(\**) замінти одну з картинок на свою

2. 5 тегів 5 класів
використовуючи blank.html за основу, підглядаючи в in_tag.html створитий свій html файл в якому:  
 буде (щонайменьше) 5 тегів та  
 5 класів 
 призначити тегам класи (можна декілька класів одному тегу)
 в класах можна використовувати (на вибір і не обмежуючись) такі властивості
 ```
background:red;
font-size:120px;

text-decoration:underline
text-decoration: overline;
text-decoration: line-through;

text-transform:uppercase
text-transform: lowercase;
text-transform: capitalize;

 ```
(*) розібратися самостійно як працює text-shadow за допомогою цього довідника  
https://www.w3schools.com/css/css_text_shadow.asp    