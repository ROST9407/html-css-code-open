1. до/після  
на прикладі robot ми подивилися як працює псевдоклас img:hover   
завдання:  
використати в img:hover атрибут background-image, що змінює картинку при наведенні на елемент.
використовуючи цей навичок зробіть сторінку яка вам цікава. 
на сторінці має бути одне зображення, і при наведенні воно має змінюватися на інше.  
ідеї: до/після, було/стало, петарда/фейерверк, милий котик/страшний тигр. але у вас можуть бути свої.
 
 2. пофіксити letter  
 поставити фоном зображення rough-paper.png.
 змінити фон тих елементів, які зливаються з фоном сторінки
 
 3. свій letter  
 відпрацьовуюємо роботу з зображенням фону та підключенням шрифтів.  
 виберіть, підключиіть та використайте щонайменьше один шрифт.  
 виберіть та використайте background-image до body та/або div.  
 ідеї: текст на папірусі, сторінка з дитячої книжки, чек з супермаркета, зелені литери на терміналі. але краще ваші і що вам цікаво